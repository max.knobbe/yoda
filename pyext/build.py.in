#! /usr/bin/env python

from __future__ import print_function
import platform, sysconfig, os, sys
from glob import glob


## Build dirs
extsrcdir = "@abs_srcdir@/yoda"
extbuilddir = "@abs_builddir@/yoda"
srcdir = os.path.abspath("@abs_top_srcdir@/src")
libdir = os.path.abspath("@abs_top_builddir@/src/.libs")
incdirs = [os.path.abspath("@abs_top_srcdir@/include"),
           os.path.abspath("@abs_top_builddir@/include"),
           os.path.abspath(extsrcdir),
           os.path.abspath(extbuilddir)]
lookupdirs = []

## Include args
incargs = " ".join("-I{}".format(d) for d in incdirs)
incargs += " -I@prefix@/include"
incargs += " @CPPFLAGS@"
incargs += " -I" + sysconfig.get_config_var("INCLUDEPY")

## Compile args
cmpargs = "@PYEXT_CXXFLAGS@"
cmpargs += " @CXXFLAGS@"

## Link args -- base on install-prefix, or on local lib dirs for pre-install build
linkargs = " ".join("-L{}".format(d) for d in lookupdirs)
linkargs += " -L@abs_top_builddir@/src/.libs" if "YODA_LOCAL" in os.environ else "-L@prefix@/lib"
linkargs += " @LDFLAGS@"

## Library args
libraries = ["YODA"]
libargs = " ".join("-l{}".format(l) for l in libraries)

## Python compile/link args
pyargs = "-I" + sysconfig.get_config_var("INCLUDEPY")
libpys = [os.path.join(sysconfig.get_config_var(ld), sysconfig.get_config_var("LDLIBRARY")) for ld in ["LIBPL", "LIBDIR"]]
libpys.extend( glob(os.path.join(sysconfig.get_config_var("LIBPL"), "libpython*.*")) )
libpys.extend( glob(os.path.join(sysconfig.get_config_var("LIBDIR"), "libpython*.*")) )
libpy = None
for lp in libpys:
    if os.path.exists(lp):
        libpy = lp
        break
if libpy is None:
    print("No libpython found in expected location exiting")
    print("Considered locations were:", libpys)
    sys.exit(1)
pyargs += " " + libpy
pyargs += " " + sysconfig.get_config_var("LIBS")
pyargs += " " + sysconfig.get_config_var("LIBM")
#pyargs += " " + sysconfig.get_config_var("LINKFORSHARED")


## (Re)make the build/yoda dir
import shutil
builddir = "@abs_builddir@/build/yoda"
try:
    shutil.rmtree(builddir)
except:
    print("Couldn't remove build/yoda/ dir")
try:
    os.makedirs(builddir)
except FileExistsError:
    pass

## Copy in Python sources
for pyfile in glob(os.path.join(extsrcdir, "*.py")):
    shutil.copy(pyfile, builddir)
## Plotting subdir
plotbuilddir = os.path.join(builddir,"plotting")
try:
    shutil.rmtree(plotbuilddir)
except:
    print("Couldn't remove build/yoda/plotting/ dir")
    pass
shutil.copytree("@srcdir@/yoda/plotting", plotbuilddir)

## Modules to build, adding rootcompat if requested
srcnames = ["core", "util"]
if "BUILD_ROOTCOMPAT" in os.environ:
    srcnames.append("rootcompat")


## Run the compilation in the build dir for each source file
# TODO: Avoid unnecessary rebuilds
for srcname in srcnames:
    ## Find the extension source file
    srcpath = os.path.join(extbuilddir, srcname+".cpp")
    if not os.path.isfile(srcpath): # distcheck has it in srcdir
        srcpath = os.path.relpath(os.path.join(extsrcdir, srcname+".cpp"))

    ## Add errors source for core only
    if srcname == "core":
        srcpath += " " + os.path.join(extsrcdir, "errors.cpp")

    ## Add ROOT args for rootcompat only
    xcmpargs, xlinkargs = cmpargs, linkargs
    if srcname == "rootcompat":
        xcmpargs += " " + "@ROOT_CXXFLAGS@"
        xlinkargs += " " + "@ROOT_LDFLAGS@ @ROOT_LIBS@"

    ## Assemble the compile & link command
    compile_cmd = "  ".join([os.environ.get("CXX", "g++"), "-shared -fPIC", "-o {}.so".format(srcname),
                             srcpath, incargs, xcmpargs, xlinkargs, libargs, pyargs])
    print("Build command =", compile_cmd)

    ## Run the extension compilation
    import subprocess
    code = subprocess.call(compile_cmd.split(), cwd=builddir)
    if code != 0:
        sys.exit(code)
